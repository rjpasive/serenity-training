package net.serenitybdd.exercise.FillUpForm_Otan.tasks;

import net.serenitybdd.exercise.FillUpForm_Otan.ui.ToolsQaAutomationPracticeForm;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Step;

public class OpenTheFillUpPracticeForm implements Task {
	
	ToolsQaAutomationPracticeForm toolsQaAutomationPracticeForm;

    @Step("Open the practice form")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Open.browserOn().the(toolsQaAutomationPracticeForm)
        );
    }
}
