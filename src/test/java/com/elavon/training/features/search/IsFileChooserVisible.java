package com.elavon.training.features.search;

import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.annotations.Subject;
import net.serenitybdd.screenplay.targets.Target;

@Subject("the file chooser element is visible")
public class IsFileChooserVisible implements Question<Boolean> {

	private Target fileChooser;
	
	public IsFileChooserVisible(Target fileChooser) {
		this.fileChooser = fileChooser;
	}
	
	@Override
	public Boolean answeredBy(Actor actor) {
		WebElementFacade element = fileChooser.resolveFor(actor);
		
		return element.isVisible();
	}

}
