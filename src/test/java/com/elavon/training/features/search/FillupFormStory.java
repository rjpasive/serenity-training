package com.elavon.training.features.search;

import static com.elavon.training.matcher.StringMatcher.contains;
import static net.serenitybdd.screenplay.GivenWhenThen.givenThat;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.GivenWhenThen.then;
import static net.serenitybdd.screenplay.GivenWhenThen.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import com.elavon.training.matcher.IsElementContainsString;
import com.elavon.training.tasks.EnterName;
import com.elavon.training.tasks.OpenFillupForm;
import com.elavon.training.ui.SearchPage;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.findby.By;

@RunWith(SerenityRunner.class)
public class FillupFormStory {

	@Managed
	public WebDriver hisBrowser;

	Actor john = Actor.named("John");

	@Steps
	OpenFillupForm openFillupForm;

	@Before
	public void annaCanBrowseTheWeb() {
		john.can(BrowseTheWeb.with(hisBrowser));
	}

	@Test
	public void fill_up_personal_info() {

		givenThat(john).wasAbleTo(openFillupForm);

		when(john).attemptsTo(Enter.theValue("C:/Users/rapasive/Array.java").into(By.id("photo")));

		when(john).attemptsTo(EnterName.withFirstname("Robin").withMiddlename("Asor").withLastname("Pasive"));

		// then(john).should(seeThat(SearchPage.fileChooserIsVisile()));

		then(john).should(seeThat(SearchPage.page_title(), contains("Practice Automation Form 123123")));

	}

}
