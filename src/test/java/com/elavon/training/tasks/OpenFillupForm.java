package com.elavon.training.tasks;

import com.elavon.training.ui.FormPage;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Step;

public class OpenFillupForm implements Task {

	FormPage formPage;

	@Step("{0} opens the form page")
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn().the(formPage));
	}

}
