package net.serenitybdd.exercise.FillUpForm_Otan.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.annotations.Subject;
import net.serenitybdd.screenplay.targets.Target;
@Subject("#target should be #subject")
public class GetStateOfElement implements Question<Boolean>{

	private Target target;
	private String option;
	@SuppressWarnings("unused")
	private String subject;
	
	@Override
	public Boolean answeredBy(Actor actor) {
		if(option == "isSelected"){
			return target.resolveFor(actor).isSelected();
		}else if (option == "ntSelected"){
			if (!target.resolveFor(actor).isSelected()){
				return true;
			} else {
				return false;
			}
		}else{
			return false;
		}
	}
	
	public GetStateOfElement(Target target, String option){
		this.target = target;
		this.option = option;
		this.subject = option.substring(2, option.length());
		if (option == "ntSelected"){
			this.subject = "not be " + option.substring(2, option.length());
		}
	}
	
	public static GetStateOfElement isSelected(Target target){
		return new GetStateOfElement(target, "isSelected");
	}
	
	public static GetStateOfElement notSelected(Target target){
		return new GetStateOfElement(target, "ntSelected");
	}
}
