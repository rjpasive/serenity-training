package com.elavon.training.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://toolsqa.com/automation-practice-form/")
public class FormPage extends PageObject {

	public static Target FIRSTNAME_FIELD = Target.the("firstname field").located(By.name("firstname"));

	public static Target MIDDLENAME_FIELD = Target.the("middlename field").located(By.id("datepicker"));

	public static Target LASTNAME_FIELD = Target.the("lastname field").located(By.name("lastname"));
}
