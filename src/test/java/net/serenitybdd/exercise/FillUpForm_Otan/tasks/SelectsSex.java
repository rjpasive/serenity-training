package net.serenitybdd.exercise.FillUpForm_Otan.tasks;

import net.serenitybdd.exercise.FillUpForm_Otan.ui.FillUpFormTargets;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.thucydides.core.annotations.Step;

public class SelectsSex implements Task{
	
	static String gender;
	
	@Step("Select gender")
	@Override
	public <T extends Actor> void performAs(T actor) {
		if (gender == "Male"){
			actor.attemptsTo(Click.on(FillUpFormTargets.SEX_MALE));
		}else if (gender == "Female"){
			actor.attemptsTo(Click.on(FillUpFormTargets.SEX_FEMALE));
		}
	}
	
	public SelectsSex (String gender){
		SelectsSex.gender = gender;
	}
	
	public static SelectsSex gender(String gender){
		return Tasks.instrumented(SelectsSex.class, gender);
	}

}
