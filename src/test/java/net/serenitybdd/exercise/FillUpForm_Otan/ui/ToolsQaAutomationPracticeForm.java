package net.serenitybdd.exercise.FillUpForm_Otan.ui;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://toolsqa.com/automation-practice-form/")
public class ToolsQaAutomationPracticeForm extends PageObject {}
