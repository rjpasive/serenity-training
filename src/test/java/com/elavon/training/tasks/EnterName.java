package com.elavon.training.tasks;

import com.elavon.training.ui.FormPage;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.Step;

public class EnterName implements Task {

	private String firstname;
	private String middlename;
	private String lastname;

	public EnterName(String firstname) {
		this.firstname = firstname;
	}
	
	public EnterName(String firstname, String lastname) {
		this.firstname = firstname;
		this.lastname = lastname;
	}
	
	public EnterName(String firstname, String middlename, String lastname) {
		this.firstname = firstname;
		this.middlename = middlename;
		this.lastname = lastname;
	}

	public static EnterName withFirstname(String firstname) {
		return new EnterName(firstname);
	}
	
	public EnterName withMiddlename(String middlename) {
		this.middlename = middlename;
		return this;
	}

	public Performable withLastname(String lastname) {
		return Tasks.instrumented(EnterName.class, firstname, middlename, lastname);
	}

	@Step("Enter name")
	@Override
	public <T extends Actor> void performAs(T actor) {

		actor.attemptsTo(Enter.theValue(firstname).into(FormPage.FIRSTNAME_FIELD));

		actor.attemptsTo(Enter.theValue(middlename).into(FormPage.MIDDLENAME_FIELD));
		
		actor.attemptsTo(Enter.theValue(lastname).into(FormPage.LASTNAME_FIELD));

	}

}
