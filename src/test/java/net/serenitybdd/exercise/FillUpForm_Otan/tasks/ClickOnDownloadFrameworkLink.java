package net.serenitybdd.exercise.FillUpForm_Otan.tasks;

import net.serenitybdd.exercise.FillUpForm_Otan.ui.FillUpFormTargets;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.thucydides.core.annotations.Step;

public class ClickOnDownloadFrameworkLink implements Task {

	private static Integer link;
	
	@Step("Click Download Link")
	@Override
	public <T extends Actor> void performAs(T actor) {
		
		if (link == 0){
			actor.attemptsTo(Click.on(FillUpFormTargets.LINK_SELENIUM_AUTOMATION_HYBRID_FRAMEWORK));
		}else{
			actor.attemptsTo(Click.on(FillUpFormTargets.TEST_FILE_TO_DOWNLOAD));
		}
		
	}
	
	public ClickOnDownloadFrameworkLink(Integer link){
		ClickOnDownloadFrameworkLink.link = link;
	}
	
	public static Performable LINK_SELENIUM_AUTOMATION_HYBRID_FRAMEWORK(){
		return Tasks.instrumented(ClickOnDownloadFrameworkLink.class,0);
	}
	
	public static Performable TEST_FILE_TO_DOWNLOAD(){
		return Tasks.instrumented(ClickOnDownloadFrameworkLink.class,1);
	}

}
