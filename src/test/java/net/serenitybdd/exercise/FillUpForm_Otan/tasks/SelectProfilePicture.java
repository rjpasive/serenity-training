package net.serenitybdd.exercise.FillUpForm_Otan.tasks;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.WebDriver;

import net.serenitybdd.exercise.FillUpForm_Otan.ui.FillUpFormTargets;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.components.FileToUpload;

public class SelectProfilePicture implements Task{

	private String path;
	private WebDriver driver;
	private Robot robot;
	
	@Step("Upload photo")
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Click.on(FillUpFormTargets.UPLOAD_PHOTO));
		try {
			robot = new Robot();
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		robot.keyPress(KeyEvent.VK_ESCAPE);
		robot.keyRelease(KeyEvent.VK_ESCAPE);
//		actor.attemptsTo(Enter.theValue(path).into(FillUpFormTargets.UPLOAD_PHOTO));
		FileToUpload fileToUpload = new FileToUpload(driver, path);
		fileToUpload.fromLocalMachine().to(FillUpFormTargets.UPLOAD_PHOTO.resolveFor(actor).getWrappedElement());
	
//		robot.keyPress(KeyEvent.VK_ESCAPE);
//		robot.keyRelease(KeyEvent.KEY_RELEASED);
	}
	
	public SelectProfilePicture(String path){
		this.path = path;
	}

	public static Performable with (WebDriver driver, String path){
		return Tasks.instrumented(SelectProfilePicture.class, path);
	}
}
