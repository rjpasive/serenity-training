package com.elavon.training.matcher;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.annotations.Subject;

public class IsElementContainsString extends BaseMatcher<String> {

	private String expectedString;

	public IsElementContainsString(String expectedString) {
		this.expectedString = expectedString;
	}

	@Override
	public boolean matches(Object obj) {
		String actualElement = (String) obj;
		
		return actualElement.contains(expectedString);
//		return expectedString.equals(actualElement.getText());
	}

	@Override
	public void describeTo(Description description) {
		description.appendText("\"" + expectedString + "\"");
	}
}
