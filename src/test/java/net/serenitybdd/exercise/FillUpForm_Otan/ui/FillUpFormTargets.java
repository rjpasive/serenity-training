package net.serenitybdd.exercise.FillUpForm_Otan.ui;

import org.openqa.selenium.By;

import net.serenitybdd.exercise.FillUpForm_Otan.tasks.GetStateOfElement;
import net.serenitybdd.exercise.FillUpForm_Otan.tasks.GetTarget_Content;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.targets.Target;

public class FillUpFormTargets {
	
	public static Target FIRST_NAME= Target.the("first name").located(By.name("firstname"));
	public static Target LAST_NAME = Target.the("last name").located(By.name("lastname"));
	public static Target SEX_MALE = Target.the("male").located(By.id("sex-0"));
	public static Target SEX_FEMALE = Target.the("female").located(By.id("sex-1"));
	public static Target DATE = Target.the("date").located(By.id("datepicker"));
	public static Target PROFESSION_MANUAL_TESTER =Target.the("manual tester").located(By.id("profession-0"));
	public static Target PROFESSION_AUTOMATION_TESTER =Target.the("automation tester").located(By.id("profession-1"));
	public static Target SELENIUM_COMMANDS = Target.the("selenium commands").located(By.id("selenium_commands"));
	public static Target UPLOAD_PHOTO = Target.the("upload photo button").located(By.id("photo"));
	public static Target CONTINENT = Target.the("continent").located(By.id("continents"));
	public static Target LINK_SELENIUM_AUTOMATION_HYBRID_FRAMEWORK = Target.the("Link Selenium Automation Hybrid Framework").located(By.linkText("Selenium Automation Hybrid Framework"));
	public static Target TEST_FILE_TO_DOWNLOAD = Target.the("Link Test File to Download").located(By.linkText("Test File to Download"));
	
	public static Question<String> first_name_content(){return new GetTarget_Content(FIRST_NAME);}
	
	public static Question<String> last_name_content(){return new GetTarget_Content(LAST_NAME);}
	
	public static Question<Boolean> theSelected_gender_is(String gender){
		if(gender=="Male"){
			return GetStateOfElement.isSelected(SEX_MALE);
		}else if (gender=="Female"){
			return GetStateOfElement.isSelected(SEX_FEMALE);
		}else{
			return null;
		}
	}
	
	public static Question<Boolean> selected_experience_is(Integer noOfYears){
		return GetStateOfElement.isSelected(
				Target.the("years of experience "+noOfYears).located(By.id("exp-" + (noOfYears-1))));
	}
	
	public static Question<String> enteredDate(){
		return new GetTarget_Content(DATE);
	}
	
	public static Question<Boolean> areYouAManualTester(String ans){
		if (ans == "Yes"){
			return GetStateOfElement.isSelected(PROFESSION_MANUAL_TESTER);
		} else {
			return GetStateOfElement.notSelected(PROFESSION_MANUAL_TESTER);
		}
	}
	public static Question<Boolean> areYouAAutomationTester(String ans){
		if (ans == "Yes"){
			return GetStateOfElement.isSelected(PROFESSION_AUTOMATION_TESTER);
		} else {
			return GetStateOfElement.notSelected(PROFESSION_AUTOMATION_TESTER);
		}
	}
}
