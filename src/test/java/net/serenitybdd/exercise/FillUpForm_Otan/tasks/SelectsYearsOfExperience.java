package net.serenitybdd.exercise.FillUpForm_Otan.tasks;

import org.openqa.selenium.By;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.Step;

public class SelectsYearsOfExperience implements Task{

	private static Integer noOfYears;
	
	@Step("Select years of experience")
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Click.on(Target.the("years of experience "+noOfYears).located(By.id("exp-" + (noOfYears-1)))));
	}
	
	public SelectsYearsOfExperience(Integer noOfYears){
		SelectsYearsOfExperience.noOfYears = noOfYears;
	}
	
	public static Performable withNumberOfYears(Integer noOfYears){
		return Tasks.instrumented(SelectsYearsOfExperience.class, noOfYears);
	}

}
