package net.serenitybdd.exercise.FillUpForm_Otan.tasks;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import net.serenitybdd.exercise.FillUpForm_Otan.ui.FillUpFormTargets;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.Step;

public class FillUpDateField implements Task{

	@Step("Fillup Data with Current Date")
	@Override
	public <T extends Actor> void performAs(T actor) {
		// TODO Auto-generated method stub
		actor.attemptsTo(Enter.theValue(DateTimeFormat.shortDate().print(DateTime.now()))
				.into(FillUpFormTargets.DATE));
		
	}
	
	public static FillUpDateField withTodaysDate (){
		return new FillUpDateField();
	}
	
}
