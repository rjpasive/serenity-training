package com.elavon.training.question;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.annotations.Subject;
import net.serenitybdd.screenplay.targets.Target;

@Subject("WebElementFacade Question")
public class WebElementFacadeQuestion implements Question<String> {

	private Target target;

	public WebElementFacadeQuestion(Target target) {
		this.target = target;
	}

	@Override
	public String answeredBy(Actor actor) {

		return target.resolveFor(actor).getText();
	}

}
