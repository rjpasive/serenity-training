package net.serenitybdd.exercise.FillUpForm_Otan.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.annotations.Subject;
import net.serenitybdd.screenplay.targets.Target;

@Subject("#target")
public class GetTarget_Content implements Question<String>{

	private Target target;
	
	@Override
	public String answeredBy(Actor actor) {

		return target.resolveFor(actor).getTextValue();
	}
	
	public GetTarget_Content(Target target){
		this.target = target;
	}

}
