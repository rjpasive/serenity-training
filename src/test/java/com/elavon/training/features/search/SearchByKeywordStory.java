package com.elavon.training.features.search;

import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.serenitybdd.screenplay.questions.page.TheWebPage;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import net.thucydides.junit.annotations.UseTestDataFrom;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import com.elavon.training.tasks.OpenTheApplication;
import com.elavon.training.tasks.Search;
import com.elavon.training.ui.SearchPage;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static net.serenitybdd.screenplay.EventualConsequence.eventually;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;

@UseTestDataFrom("src/test/resources/input.csv")
@RunWith(SerenityParameterizedRunner.class)
public class SearchByKeywordStory {

	public String searhKey;
	public String col2;
	public String col3;
	
    Actor anna = Actor.named("Anna");

    @Managed
    public WebDriver herBrowser;

    @Steps
    OpenTheApplication openTheApplication;

    @Before
    public void annaCanBrowseTheWeb() {
        anna.can(BrowseTheWeb.with(herBrowser));
    }

    @Test
    @Title("This is my first test")
    public void search_results_should_show_the_search_term_in_the_title() {

    	System.out.println(searhKey);
    	System.out.println(col2);
    	System.out.println(col3);
    	
        givenThat(anna).wasAbleTo(openTheApplication);

        when(anna).attemptsTo(Search.forTheTerm(searhKey));
        
//        anna.attemptsTo(DoubleClick.on(SearchPage.LUCKY_BUTTON));
        
//        anna.attemptsTo(Enter.theValue("search key").into(SearchPage.SEARCH_FIELD));

        then(anna).should(eventually(seeThat(TheWebPage.title(), containsString(searhKey))));

    }
    
}