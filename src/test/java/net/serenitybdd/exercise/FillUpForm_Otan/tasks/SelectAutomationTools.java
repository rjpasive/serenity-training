package net.serenitybdd.exercise.FillUpForm_Otan.tasks;

import org.openqa.selenium.By;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.Step;

public class SelectAutomationTools implements Task{

	private static String[] tools;
	
	@Step("Select automation tool")
	@Override
	public <T extends Actor> void performAs(T actor) {
		// TODO Auto-generated method stub
		for (String temp : tools){
			actor.attemptsTo(Click.on(Target.the(temp).located(By.xpath("//input[@value='" + temp + "']"))));
		}
	}
	
	public static Performable with(String... tools){
		SelectAutomationTools.tools = new String[tools.length];
		Integer i = 0;
		String idk = null;
		for (String temp : tools){
			SelectAutomationTools.tools[i] = temp;
			idk = idk + ", " + temp;
			i+=1;
		}
		return Tasks.instrumented(SelectAutomationTools.class, idk);
	}

}
