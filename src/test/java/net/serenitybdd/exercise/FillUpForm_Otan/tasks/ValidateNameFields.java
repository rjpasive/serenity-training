package net.serenitybdd.exercise.FillUpForm_Otan.tasks;

import static net.serenitybdd.exercise.FillUpForm_Otan.ui.FillUpFormTargets.first_name_content;
import static net.serenitybdd.exercise.FillUpForm_Otan.ui.FillUpFormTargets.last_name_content;
import static net.serenitybdd.screenplay.EventualConsequence.eventually;
import static net.serenitybdd.screenplay.GivenWhenThen.and;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.GivenWhenThen.then;
import static org.hamcrest.Matchers.containsString;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.thucydides.core.annotations.Step;

public class ValidateNameFields implements Task{

	private static String firstName;
	private static String lastName;
	
	public ValidateNameFields(String firstName) {
		ValidateNameFields.firstName = firstName;
	}
	
	public ValidateNameFields(String firstName, String lastName) {
		ValidateNameFields.firstName = firstName;
		ValidateNameFields.lastName = lastName;
	}

	@Step("Validation of content of name fields")
	@Override
	public <T extends Actor> void performAs(T actor) {
		
		then(actor).should(eventually(seeThat(first_name_content(), containsString(firstName))));
        
    	and(actor).should(eventually(seeThat(last_name_content(), containsString(lastName))));
		
	}
	
	public static ValidateNameFields withFirstName(String firstName){
		return new ValidateNameFields(firstName);
	}
	
	public Performable withLastName(String lastName){
		return Tasks.instrumented(ValidateNameFields.class, firstName, lastName);
	}

}
