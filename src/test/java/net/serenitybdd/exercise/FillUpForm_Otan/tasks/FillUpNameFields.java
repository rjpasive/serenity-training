package net.serenitybdd.exercise.FillUpForm_Otan.tasks;

//import static net.serenitybdd.screenplay.GivenWhenThen.then;
import net.serenitybdd.exercise.FillUpForm_Otan.ui.FillUpFormTargets;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.Step;

public class FillUpNameFields implements Task {

	private static String firstName;
	private static String lastName;
	
	public FillUpNameFields(String firstName) {
		FillUpNameFields.firstName = firstName;
	}
	
	public FillUpNameFields(String firstName, String lastName){
		FillUpNameFields.firstName = firstName;
		FillUpNameFields.lastName = lastName;
	}
	
	@Step("Fill up name fields")
	@Override
	public <T extends Actor> void performAs(T actor) {
		// TODO Auto-generated method stub
		actor.attemptsTo(Enter.theValue(firstName).into(FillUpFormTargets.FIRST_NAME));
		actor.attemptsTo(Enter.theValue(lastName).into(FillUpFormTargets.LAST_NAME));
//		then(actor).attemptsTo(ValidateNameFields.withFirstName("othan").withLastName(lastName));
	}
	
	public static FillUpNameFields withFirstName(String firstName){
		return new FillUpNameFields(firstName);
	}
	
	public Performable withLastName(String lastName){
		return Tasks.instrumented(FillUpNameFields.class, firstName, lastName);
	}

}
