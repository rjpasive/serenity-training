package net.serenitybdd.exercise.FillUpForm_Otan.tasks;

import net.serenitybdd.exercise.FillUpForm_Otan.ui.FillUpFormTargets;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.thucydides.core.annotations.Step;

public class AnswerWhatIsYourProfession implements Task{
	
	private static String manualTester;
	private static String automationTester;
	
	@Step("Select Profession")
	@Override
	public <T extends Actor> void performAs(T actor) {
		if (manualTester == "Yes"){
		actor.attemptsTo(Click.on(FillUpFormTargets.PROFESSION_MANUAL_TESTER));
		}
		if(automationTester == "Yes"){
		actor.attemptsTo(Click.on(FillUpFormTargets.PROFESSION_AUTOMATION_TESTER));
		}
	}
	
	public AnswerWhatIsYourProfession(String manualTester, String automationTester){
		AnswerWhatIsYourProfession.manualTester = manualTester;
		AnswerWhatIsYourProfession.automationTester = automationTester;
	}

	public AnswerWhatIsYourProfession(String manualTester) {
		AnswerWhatIsYourProfession.manualTester = manualTester;
	}

	public static AnswerWhatIsYourProfession areYouAManualTester(String manualTester){
		return new AnswerWhatIsYourProfession(manualTester);
	}
	
	public Performable areYouAAutomationTester(String automationTester){
		return Tasks.instrumented(AnswerWhatIsYourProfession.class, manualTester, automationTester);
	}
	
}
