package net.serenitybdd.exercise.FillUpForm_Otan.tasks;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import net.serenitybdd.exercise.FillUpForm_Otan.ui.FillUpFormTargets;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.thucydides.core.annotations.Step;

public class SelectSeleniumCommands implements Task {

	private static String[] tempCommand;
	private static String idk;
	@Step("Select selenium commands")
	@Override
	public <T extends Actor> void performAs(T actor) {
		Robot robot = null;
		try {
			robot = new Robot();
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for(String tmp : tempCommand){
			robot.keyPress(KeyEvent.VK_CONTROL);
			
			actor.attemptsTo(SelectFromOptions.byVisibleText(tmp)
					.from(FillUpFormTargets.SELENIUM_COMMANDS));
			
			robot.keyRelease(KeyEvent.VK_CONTROL);
		}
	}
	
	public static Performable withCommands (String... commands){
		System.out.println(commands.length);
		tempCommand = new String[commands.length];
		Integer i = 0;
		for (String command : commands){
			System.out.println(command);
			tempCommand[i] = command;
			i += 1;
			idk = idk + ", " + command;
		}
		return Tasks.instrumented(SelectSeleniumCommands.class, idk);
	}
	

	
	
	
}
