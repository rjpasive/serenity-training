package net.serenitybdd.exercise.FillUpForm_Otan;

import static net.serenitybdd.exercise.FillUpForm_Otan.ui.FillUpFormTargets.areYouAAutomationTester;
import static net.serenitybdd.exercise.FillUpForm_Otan.ui.FillUpFormTargets.areYouAManualTester;
import static net.serenitybdd.exercise.FillUpForm_Otan.ui.FillUpFormTargets.enteredDate;
import static net.serenitybdd.exercise.FillUpForm_Otan.ui.FillUpFormTargets.selected_experience_is;
import static net.serenitybdd.exercise.FillUpForm_Otan.ui.FillUpFormTargets.theSelected_gender_is;
import static net.serenitybdd.screenplay.EventualConsequence.eventually;
import static net.serenitybdd.screenplay.GivenWhenThen.givenThat;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.GivenWhenThen.then;
import static net.serenitybdd.screenplay.GivenWhenThen.when;
import static org.hamcrest.Matchers.containsString;
import net.serenitybdd.exercise.FillUpForm_Otan.tasks.AnswerWhatIsYourProfession;
import net.serenitybdd.exercise.FillUpForm_Otan.tasks.ClickOnDownloadFrameworkLink;
import net.serenitybdd.exercise.FillUpForm_Otan.tasks.FillUpDateField;
import net.serenitybdd.exercise.FillUpForm_Otan.tasks.FillUpNameFields;
import net.serenitybdd.exercise.FillUpForm_Otan.tasks.OpenTheFillUpPracticeForm;
import net.serenitybdd.exercise.FillUpForm_Otan.tasks.SelectAutomationTools;
import net.serenitybdd.exercise.FillUpForm_Otan.tasks.SelectContinent;
import net.serenitybdd.exercise.FillUpForm_Otan.tasks.SelectProfilePicture;
import net.serenitybdd.exercise.FillUpForm_Otan.tasks.SelectSeleniumCommands;
import net.serenitybdd.exercise.FillUpForm_Otan.tasks.SelectsSex;
import net.serenitybdd.exercise.FillUpForm_Otan.tasks.SelectsYearsOfExperience;
import net.serenitybdd.exercise.FillUpForm_Otan.tasks.ValidateNameFields;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.annotations.Subject;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
public class FillUpForm {
		
	 	Actor than = Actor.named("Than");
	 	String dateToday = DateTimeFormat.shortDate().print(DateTime.now());
		
	    @Managed(uniqueSession = true)
	    public WebDriver hisBrowser;

	    @Steps
	    OpenTheFillUpPracticeForm openTheFillUpPracticeForm;
	    
	    @Before
	    public void thanCanBrowseTheWeb() {
	        than.can(BrowseTheWeb.with(hisBrowser));
	    }
	    
	    @Subject("Load the website")
	    @Test
	    public void load_the_website(){
	        
	    	
	        
	    }
	    
	    @Subject("Fill up all fields in the application form")
	    @Test
	    public void fill_up_form_fields(){
	    	
	    	givenThat(than).wasAbleTo(openTheFillUpPracticeForm);
	        
	        when(than).attemptsTo(FillUpNameFields.withFirstName("Jonathan").withLastName("Agustin"));
	        
	        	then(than).attemptsTo(ValidateNameFields.withFirstName("Jonathan").withLastName("Agustin"));
	        
	        when(than).attemptsTo(SelectsSex.gender("Male"));
	        	
	        	then(than).should(eventually(seeThat(theSelected_gender_is("Male"))));
	        	
	        when(than).attemptsTo(SelectsYearsOfExperience.withNumberOfYears(1));
	        
        		then(than).should(eventually(seeThat(selected_experience_is(1))));
        		
	        when(than).attemptsTo(FillUpDateField.withTodaysDate());
        	
        		then(than).should(eventually(seeThat(enteredDate(), containsString(dateToday))));
        		
   	    	when(than).attemptsTo(AnswerWhatIsYourProfession.areYouAManualTester("Yes")
		        		.areYouAAutomationTester("No"));
		        
		        then(than).should(eventually(seeThat(areYouAManualTester("Yes"))));
		        	
		        then(than).should(eventually(seeThat(areYouAAutomationTester("No"))));
		        
		    when(than).attemptsTo(SelectProfilePicture.with(hisBrowser,"C:/Users/Public/Pictures/Sample Pictures/Koala.jpg"));
	    	
		    when(than).attemptsTo(ClickOnDownloadFrameworkLink.LINK_SELENIUM_AUTOMATION_HYBRID_FRAMEWORK());
	    	
	    	when(than).attemptsTo(ClickOnDownloadFrameworkLink.LINK_SELENIUM_AUTOMATION_HYBRID_FRAMEWORK());

	    	when(than).attemptsTo(SelectAutomationTools.with("QTP","Selenium IDE","Selenium Webdriver"));
	    	
	    	when(than).attemptsTo(SelectContinent.withContinent("North America"));

	    	when(than).attemptsTo(SelectSeleniumCommands.withCommands(
	    			"Browser Commands",
	    			"Navigation Commands",
	    			"WebElement Commands")
	    			);
	    }
	    
//	    @Subject("Fill up gender fields")
//	    @Test
//	    public void fill_up_gender_fields(){
//	        
//	    	when(than).attemptsTo(SelectsSex.gender("Male"));
//	        	
//	        	then(than).should(eventually(seeThat(theSelected_gender_is("Male"))));
//	        	
//	    }
	    
//	    @Subject("Fill up years of experience fields")
//	    @Test
//	    public void fill_up_years_of_experience_fields(){
//	        
//	        when(than).attemptsTo(SelectsYearsOfExperience.withNumberOfYears(1));
//	        
//        		then(than).should(eventually(seeThat(selected_experience_is(1))));
//        
//	    }
	    
//	    @Subject("Fill up date fields")
//	    @Test
//	    public void fill_up_date_fields(){
//	        
//	        when(than).attemptsTo(FillUpDateField.withTodaysDate());
//        	
//        		then(than).should(eventually(seeThat(enteredDate(), containsString(dateToday))));
//        
//	    }
	    
//	    @Subject("Fill up profession fields")
//	    @Test
//	    public void fill_up_profession_fields(){
//	        
//	    	 when(than).attemptsTo(AnswerWhatIsYourProfession.areYouAManualTester("Yes")
//		        		.areYouAAutomationTester("No"));
//		        
//		        then(than).should(eventually(seeThat(areYouAManualTester("Yes"))));
//		        	
//		        then(than).should(eventually(seeThat(areYouAAutomationTester("No"))));
//   
//	    }
	    
//	    @Subject("Upload photo")
//	    @Test
//	    public void upload_photo(){
//	    	
//	    	when(than).attemptsTo(SelectProfilePicture.with(hisBrowser,"C:/Users/Public/Pictures/Sample Pictures/Koala.jpg"));
//	    	
//	    }
	    
//	    @Subject("Click download Framework links")
//	    @Test
//	    public void click_download_framework(){
//	        
//	    	when(than).attemptsTo(ClickOnDownloadFrameworkLink.LINK_SELENIUM_AUTOMATION_HYBRID_FRAMEWORK());
//	    	
//	    	when(than).attemptsTo(ClickOnDownloadFrameworkLink.LINK_SELENIUM_AUTOMATION_HYBRID_FRAMEWORK());
//	    	
//	    }
	    
//	    @Subject("Select automation tools")
//	    @Test
//	    public void select_automation_tools(){
//	    	
//	    	when(than).attemptsTo(SelectAutomationTools.with("QTP","Selenium IDE","Selenium Webdriver"));
//	    	
//	    }
	    
//	    @Subject("Upload continent")
//	    @Test
//	    public void select_continent(){
//	    	
//	    	when(than).attemptsTo(SelectContinent.withContinent("North America"));
//
//	    }
	    
//	    @Subject("Fill up selenium commands field")
//	    @Test
//	    public void select_selenium_commands(){
//	        
//	    	when(than).attemptsTo(SelectSeleniumCommands.withCommands(
//	    			"Browser Commands",
//	    			"Navigation Commands",
//	    			"WebElement Commands")
//	    			);
//	    }

}
