package net.serenitybdd.exercise.FillUpForm_Otan.tasks;

import net.serenitybdd.exercise.FillUpForm_Otan.ui.FillUpFormTargets;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.thucydides.core.annotations.Step;

public class SelectContinent implements Task{
	
	private String continent;
	
	@Step("Select Continent")
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(SelectFromOptions.byVisibleText(continent).from(FillUpFormTargets.CONTINENT));
	}
	
	public SelectContinent(String continent){
		this.continent = continent;
	}

	public static Performable withContinent (String continent){
		return Tasks.instrumented(SelectContinent.class, continent);
	}
}
