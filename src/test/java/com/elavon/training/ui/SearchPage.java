package com.elavon.training.ui;

import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

import com.elavon.training.features.search.IsFileChooserVisible;
import com.elavon.training.question.WebElementFacadeQuestion;

public class SearchPage {
    public static Target SEARCH_FIELD = Target.the("search field").located(By.name("q"));
    
    public static Target SEARCH_BUTTON = Target.the("search button").located(By.name("btnK"));
    
    public static Target LUCKY_BUTTON = Target.the("search button").located(By.name("btnI"));
    
    public static Target LOGO = Target.the("logo").located(By.cssSelector("img[src='/images/branding/googlelogo/2x/googlelogo_color_120x44dp.png']"));
 
    public static Target FILE_CHOOSER = Target.the("file chooser").located(By.id("photo"));
    
    public static Target PAGE_TITLE = Target.the("page title").located(By.cssSelector("div[class='wpb_wrapper'] h1"));
    
    public static Question<String> page_title(){
    	return new WebElementFacadeQuestion(PAGE_TITLE);
//    	return actor -> PAGE_TITLE.resolveFor(actor);
    }
    
    public static Question<Boolean> fileChooserIsVisile(){
    	return new IsFileChooserVisible(SearchPage.FILE_CHOOSER);
    }
}
