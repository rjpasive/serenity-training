package com.elavon.training.matcher;

import org.hamcrest.BaseMatcher;

import net.serenitybdd.core.pages.WebElementFacade;

public class StringMatcher {

	public static BaseMatcher<String> contains(String expected) {
		return new IsElementContainsString(expected);
	}
}
